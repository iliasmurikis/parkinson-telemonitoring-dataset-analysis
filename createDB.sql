CREATE DATABASE "Parkinson"

CREATE TABLE parkinson_data (
	id integer,
	subject integer,
	age integer,
	sex integer,
	test_time real,
	motor_UPDRS real,
	total_UPDRS real,
	jitter_percent real,
	jitter_abs double precision,
	jitter_rap real,
	jitter_ppq5 real,
	jitter_ddp real,
	shimmer real,
	shimmer_db real,
	shimmer_apq3 real,
	shimmer_apq5 real,
	shimmer_apq11 real,
	shimmer_dda real,
	nhr real,
	hnr real,
	rpde real,
	dfa real,
	ppe real,
	primary key(id)
);

-- just a random query
select n.subject,n.count
from(
	select subject,count(subject)
	from parkinson_data
	group by subject) as n
order by n.subject asc;

