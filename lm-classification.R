library(DBI)
library(corrplot)
library(MASS)
library(VGAM)
library(caret)
library(outliers)
require("RPostgreSQL")

# Set details of the conneciton
con <- dbConnect(dbDriver("PostgreSQL"),
                 dbname = "Parkinson",
                 user    = "postgres",
                 password  = "password",
                 host = "localhost",
                 port = 5432)

# Check if the table exists
dbExistsTable(con, "parkinson_data")
# Execute query to get the db
select.all <- dbSendQuery(con,'select * from parkinson_data')
# Fetch the entire db
all <- dbFetch(select.all)

# Drop two first columns
all <- all[,3:ncol(all)]

# Transforming the dependent variable to a factor
all$sex[all$sex == 0] <- 'Male'
all$sex[all$sex == 1] <- 'Female'
all$sex <- factor(all$sex)

# Set the mean of each column
cols.means <- apply(all,2,mean)
# Replace NAs with the mean of each column
for (i in 2:length(all)){
  for (row in 1:nrow(all)){
    if (is.na(all(i,row))){
      all(i,row) <- cols.means(i)
    }
  }
}

# Replace outliers with mean
for (i in 1:length(all)){
  if (i == 2){next}  # Ignore field of gender
  if (i == 5){next}  # Ignore field of total_updrs
  all[,i] <- rm.outlier(all[,i], fill = TRUE)
}

# Correlation matrix
# corrplot(cor(all), method = "color")

all$total_updrs[all$total_updrs <= 40] <- 1
all$total_updrs[all$total_updrs > 40] <- 0


# Keep a 10% of the data for later use
set.seed(1234)
ind <- sample(2, nrow(all), replace = T, prob = c(0.9, 0.1))
train.set<- all[ind == 1,]
test.set <- all[ind == 2,]

# Linear Classification
relation <- lm(total_updrs~., data = train.set)
anova(relation)
attributes(relation)
summary(relation)
par(mfrow = c(2,2))
plot(relation)

train.set$total_updrs[train.set$total_updrs == 0] <- 'Danger'
train.set$total_updrs[train.set$total_updrs == 1] <- 'Safe'
train.set$total_updrs <- factor(train.set$total_updrs)
str(train.set)
test.set$total_updrs[test.set$total_updrs == 0] <- 'Danger'
test.set$total_updrs[test.set$total_updrs == 1] <- 'Safe'
test.set$total_updrs <- factor(test.set$total_updrs)
str(test.set)


pred <- predict(relation, newdata = test.set)
# confusionMatrix(pred, test.set$total_updrs)

p_class <- ifelse(pred>0.82,"Danger","Safe")
table(p_class,test.set$total_updrs)

