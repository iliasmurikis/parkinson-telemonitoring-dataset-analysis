library(DBI)
library(MASS)
library(outliers)
library(arules)
library(arulesViz)
library(clue)
library(Metrics)
library(caret)
require("RPostgreSQL")

# Set details of the conneciton
con <- dbConnect(dbDriver("PostgreSQL"),
                 dbname = "Parkinson",
                 user    = "postgres",
                 password  = "password",
                 host = "localhost",
                 port = 5432)

# Check if the table exists
dbExistsTable(con, "parkinson_data")
# Execute query to get the db
select.all <- dbSendQuery(con,'select * from parkinson_data')
# Fetch the entire db
all <- dbFetch(select.all)

# Drop two first columns
all <- all[,3:ncol(all)]

# Transforming the dependent variable to a factor
all$sex[all$sex == 0] <- 'Male'
all$sex[all$sex == 1] <- 'Female'
all$sex <- factor(all$sex)
str(all)

# Calculate the mean of each column
cols.means <- apply(all,2,mean)
# Replace NAs with the mean of each column
for (i in 2:length(all)){ # Except the sex column which is the 1st
  for (row in 1:nrow(all)){
    if (is.na(all(i,row))){
      all(i,row) <- cols.means(i)
    }
  }
}

# Replace outliers with mean
for (i in 1:length(all)){
  if (i == 2){next}  # Ignore field of gender
  all[,i] <- rm.outlier(all[,i], fill = TRUE)
}

percent <- c("10%","20%","30%","40%","50%","60%","70%","80%","90%","100%")
names <- c("KNN-Classification","LM-Classification","KNN-Regression","LM-Regression","KMeans-Clustering","Hierarchical-Clustering","Apriori Associaition Analysis")

# Create a matrix with the final values
scores <- matrix(0, nrow = 7, ncol = 10, dimnames = list(names, percent))

# Rounding function for every base
mround <- function(x,base){ 
  base*round(x/base) 
}

# Repeat for each percent
for (i in 1:10){
  # Keep i*10%
  set.seed(1234)
  prob <- (i*10)/100
  ind <- sample(2, nrow(all), replace = T, prob = c(prob, 1-prob))
  all.new <- all[ind == 1,]
  
  ###################################################################
  
  # Step 3
  
  #KNN Classification
  
  # Remove test_time, jitter_rap and jitter_ddp and run again
  all.new <- all.new[,-3]
  all.new <- all.new[,-7]
  all.new <- all.new[,-8]

  # Split data
  set.seed(1234)
  ind.new <- sample(2, nrow(all.new), replace = T, prob = c(0.9, 0.1))
  train.set<- all.new[ind.new == 1,]
  test.set <- all.new[ind.new == 2,]

  # Apply KNN Model for classificaiton
  trControl <- trainControl(method = "repeatedcv",
                            number = 10,
                            repeats = 3,
                            classProbs = TRUE,
                            summaryFunction = twoClassSummary,
                            verboseIter = TRUE)

  set.seed(222)
  fit <- train(sex ~ .,
               data = train.set,
               method = 'knn',
               tuneLength = 20,
               trControl = trControl,
               preProc = c("center", "scale"),
               metric = "ROC",
               tuneGrid = expand.grid(k = 1:60))

  # Evaluate classification
  pred <- predict(fit, newdata = test.set)
  cm <- confusionMatrix(pred, test.set$sex)
  overall.accuracy <- cm$overall['Accuracy']
  scores[1,i] <- overall.accuracy
  #----------------------------------------------------------#
  # Reset dataset
  all.new <- all[ind == 1,]
  #----------------------------------------------------------#

  # Linear Model Classification

  # Convert sex factor to numeric
  all.new$sex <- as.numeric(all.new$sex)

  # Keep a 10% of the data for later use
  set.seed(1234)
  ind.new <- sample(2, nrow(all.new), replace = T, prob = c(0.9, 0.1))
  train.set<- all.new[ind.new == 1,]
  test.set <- all.new[ind.new == 2,]

  # Linear Classification
  relation <- lm(sex~., data = train.set)
  # anova(relation)
  # attributes(relation)
  # summary(relation)

  pred <- predict(relation, newdata = test.set)
  pred <- ifelse(pred>=2,2,1)
  lm.class.rmse.score <-rmse(pred, test.set$sex)

  scores[2,i] <- lm.class.rmse.score
  #----------------------------------------------------------#
  # Reset dataset
  all.new <- all[ind == 1,]
  #----------------------------------------------------------#

  # KNN Regression

  # Remove test_time, sex and jitter_ppq5
  all.new <- all.new[,-3]
  all.new <- all.new[,-2]
  all.new <- all.new[,-7]

  # Keep a 10% of the data for later use
  set.seed(1234)
  ind.new <- sample(2, nrow(all.new), replace = T, prob = c(0.9, 0.1))
  train.set<- all.new[ind.new == 1,]
  test.set <- all.new[ind.new == 2,]

  trControl <- trainControl(method = 'repeatedcv',
                            number = 10,
                            repeats = 3,
                            verboseIter = TRUE)
  # Apply knn regression
  fit <- train(total_updrs ~.,
                data = train.set,
                tuneGrid = expand.grid(k=1:70),
                method = 'knn',
                metric = 'Rsquared',
                trControl = trControl,
                preProc = c('center', 'scale'))

  # Model Performance
  pred <- predict(fit, newdata = test.set)
  rmse.score <- RMSE(pred, test.set$total_updrs)
  scores[3,i] <- rmse.score

  #----------------------------------------------------------#
  # Reset dataset
  all.new <- all[ind == 1,]
  #----------------------------------------------------------#

  # Linear Model Regression

  # Keep a 10% of the data for later use
  set.seed(1234)
  ind.new <- sample(2, nrow(all.new), replace = T, prob = c(0.9, 0.1))
  train.set<- all.new[ind.new == 1,]
  test.set <- all.new[ind.new == 2,]

  relation <- lm(total_updrs ~ age + sex + test_time + motor_updrs + jitter_percent +
                    jitter_abs + jitter_rap + shimmer_apq3 + shimmer_apq5 + shimmer_apq11 +
                    nhr + hnr + rpde + dfa + ppe, data = train.set)

  pred <- predict(relation,newdata=test.set)
  lm.rmse <- RMSE(pred, test.set$total_updrs)
  scores[4,i] <- lm.rmse

  ###################################################################
  # Step4

  #----------------------------------------------------------#
  # Reset dataset
  all.new <- all[ind == 1,]
  #----------------------------------------------------------#

  # Clustering

  # K-means clustering
  # Set seed
  set.seed(1023)
  all.temp <- all.new
  # Remove the column of the sex
  all.temp <- all.temp[,-2]
  kmeans.result <- kmeans(all.temp, 2)
  # Evaluate clustering
  rmse.score.clust <- rmse(as.numeric(kmeans.result$cluster), as.numeric(all.new$sex))
  scores[5,i] <- rmse.score.clust

  #----------------------------------------------------------#
  # Reset dataset
  all.new <- all[ind == 1,]
  #----------------------------------------------------------#

  # Hierarchical Clustering

  set.seed(2023)
  all.temp <- all.new
  # Remove class label
  all.temp <- all.temp[,-2]
  # hierarchical clustering
  hc <- hclust(dist(all.temp), method = "ave")

  # Get cluster IDs
  groups <- cutree(hc, k = 2)
  # Evaluate clusteing
  rmse.score.clust2 <- rmse(as.numeric(groups), as.numeric(all.new$sex))
  scores[6,i] <- rmse.score.clust2
  scores[6,i] <- rmse.score.clust2
  
  ###################################################################
  # Step 5
  
  #----------------------------------------------------------#
  # Reset dataset
  all.new <- all[ind == 1,]
  #----------------------------------------------------------#
  
  # Factorize risk of the score
  all.new$total_updrs[all.new$total_updrs <= 40] <- 0
  all.new$total_updrs[all.new$total_updrs > 40] <- 1
  all.new$total_updrs[all.new$total_updrs == 0] <- 'Danger'
  all.new$total_updrs[all.new$total_updrs == 1] <- 'Safe'
  all.new$total_updrs <- factor(all.new$total_updrs)
  
  # Factorize age
  all.new$age <- factor(all.new$age)
  
  # Remove motor_updrs and all after total_updrs
  all.new <- all.new[,-4]
  all.new <- all.new[,1:4]
  
  # Round test_time
  all.new$test_time <- mround(all.new$test_time,5)
  all.new$test_time <- ifelse(all.new$test_time<0,5,all.new$test_time)
  
  # Factorize test_time
  all.new$test_time <- factor(all.new$test_time)

  # Apriori association rules mining
  rules.all <- apriori(all.new)
  inspect(rules.all)
  quality(rules.all) <- round(quality(rules.all), digits=3)
  rules.sorted <- sort(rules.all, by="lift")

  rules.prunned <- rules.sorted[1]
  
  lift.score <- rules.prunned@quality[["lift"]]
  scores[7,i] <- lift.score
}

# Keep only 3 decimals
scores <- round(scores, digits = 3)

# Plotting
x1 = seq(1,10,1)
y1 = scores[1,]    # Scores
plot(x1, y1, pch=".", ylim=c(0,4.2),xlab="Sample Size(%)", ylab="Score" , xaxt = "n",lty = 1, 
     main = "Comparison plot for percentages of the dataset")
axis(1, at=x1, labels = percent)
lines(x1,y1, col = "red")

y1 = scores[2,]
lines(x1, y1, pch=5, ylim=c(0,5),xlab="Sample Size(%)", ylab="Score" , xaxt = "n", col = "green")

y1 = scores[3,]
lines(x1, y1, pch=5, ylim=c(0,5),xlab="Sample Size(%)", ylab="Score" , xaxt = "n", col = "blue")

y1 = scores[4,]
lines(x1, y1, pch=5, ylim=c(0,5),xlab="Sample Size(%)", ylab="Score" , xaxt = "n", col = "yellow")

y1 = scores[5,]
lines(x1, y1, pch=5, ylim=c(0,5),xlab="Sample Size(%)", ylab="Score" , xaxt = "n", col = "cyan")

y1 = scores[6,]
lines(x1, y1, pch=5, ylim=c(0,5),xlab="Sample Size(%)", ylab="Score" , xaxt = "n", col = "purple")

y1 = scores[7,]
lines(x1, y1, pch=5, ylim=c(0,5),xlab="Sample Size(%)", ylab="Score" , xaxt = "n", col = "navy")

# Create the legend
legend(1, 2.8, legend = names,
       col=c("red", "green", "blue", "yellow", "cyan", "purple", "navy"), lty=1:2, cex=0.6)

