library(DBI)
library(outliers)
require("RPostgreSQL")

# Set details of the conneciton
con <- dbConnect(dbDriver("PostgreSQL"),
                 dbname = "Parkinson",
                 user    = "postgres",
                 password  = "password",
                 host = "localhost",
                 port = 5432)

# Check if the table exists
dbExistsTable(con, "parkinson_data")
# Execute query to get the db
select.all <- dbSendQuery(con,'select * from parkinson_data')
# Fetch the entire db
all <- dbFetch(select.all)

# Drop two first columns
all <- all[,3:ncol(all)]

# Calculate the mean of each column
cols.means <- apply(all,2,mean)
# Replace NAs with the mean of each column
for (i in 2:length(all)){
  for (row in 1:nrow(all)){
    if (is.na(all(i,row))){
      all(i,row) <- cols.means(i)
    }
  }
}

# Transforming the dependent variable to a factor
all$sex[all$sex == 0] <- 'Male'
all$sex[all$sex == 1] <- 'Female'
all$sex <- factor(all$sex)
str(all)

# Replace outliers with mean
for (i in 1:length(all)){
  if (i == 2){next}  # Ignore field of gender
  all[,i] <- rm.outlier(all[,i], fill = TRUE)
}

# K-Means Clustering
# Set seed
set.seed(1023)
all2 <- all
# Remove the column of the sex
all2 <- all2[,-2]
kmeans.result <- kmeans(all2, 2)

# Present the clustering results
table(all$sex, kmeans.result$cluster)

plot(all2[c("test_time", "total_updrs")], col = kmeans.result$cluster)
points(kmeans.result$centers[, c("Motor UPDRS", "Total UPDRS")],
       col = 3:4, pch = 8, cex = 2) # plot cluster centers

#####################################################################
# Hierarhichal clustering
set.seed(2023)
all3 <- all
# remove class label
all3 <- all3[,-2]
# hierarchical clustering
hc <- hclust(dist(all3), method = "ave")
# plot clusters
plot(hc, hang = -1, labels = all$sex)
# cut tree into 2 clusters
rect.hclust(hc, k = 2)
# get cluster IDs
groups <- cutree(hc, k = 2)
table(all$sex, groups)
